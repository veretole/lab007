package eu.cz.cvut.bietjv.orm.service;

import eu.cz.cvut.bietjv.orm.dao.ProductDao;
import eu.cz.cvut.bietjv.orm.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Component
public class ProductService {
    @Autowired
    private ProductDao productDao;

    @Transactional
    public void add(Product product) {
        productDao.persist(product);
    }

    @Transactional
    public void addAll(Collection<Product> products) {
        for (Product product : products) {
            productDao.persist(product);
        }
    }

    @Transactional(readOnly = true)
    public List<Product> listAll() {
        return productDao.findAll();

    }

}